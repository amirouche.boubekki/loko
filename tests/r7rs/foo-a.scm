;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: EUPL-1.2+
(define FOO-A
  (begin
    (set! counter (+ counter 1))
    counter))
