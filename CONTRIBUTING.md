<!--
SPDX-FileCopyrightText: 2022 Göran Weinholt <goran@weinholt.se>

SPDX-License-Identifier: EUPL-1.2+
-->

# Submitting code

Contributions are accepted under the terms of the EUPL, the main
license of the project.

Use one of these options:

1. Open a merge request on GitLab (https://gitlab.com/weinholt/loko);

2. Push your code to any public git repository and
   email [goran@weinholt.se](mailto:goran@weinholt.se) asking for a
   branch to be merged; or

3. Use `git format-patch` and email it
   to [goran@weinholt.se](mailto:goran@weinholt.se).

This project follows
the [REUSE specification](https://reuse.software/). Please run `reuse
lint` on your work with a recent (>2022-02) copy of the reuse tool.

If you want to share something smallish that doesn't really belong
anywhere then you can add it to the `contrib/` directory.

# Issues

There is no public issue tracker. There are ephemeral channels for
discussion, see [README.md](README.md).
