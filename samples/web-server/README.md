<!--
SPDX-FileCopyrightText: 2022 Göran Weinholt <goran@weinholt.se>

SPDX-License-Identifier: EUPL-1.2+
-->

# Basic web server demo for Loko

This program demonstrates how to use the fibers and Linux syscall
libraries to implement a simple web server.
