#!/usr/bin/env scheme-script
;; SPDX-License-Identifier: EUPL-1.2+
;; Copyright © 2019 Göran Weinholt
(import (rnrs) (hello-lib))

(hello "World")
